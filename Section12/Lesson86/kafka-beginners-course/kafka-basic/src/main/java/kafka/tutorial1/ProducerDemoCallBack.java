package kafka.tutorial1;

import java.util.Properties;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProducerDemoCallBack {

	public static void main(String[] args) {
		// Creating a logger
		final Logger logger = LoggerFactory.getLogger(ProducerDemoCallBack.class);

		// create producer properties.
		Properties properties = new Properties();
		/*
		 * properties.setProperty("bootstrap.servers", "127.0.0.1:9092");
		 * properties.setProperty("key.serializer",StringSerializer.class.getName());
		 * properties.setProperty("value.serializer", StringSerializer.class.getName());
		 */
		// USING CONSTANTS IN PRODUCER_CONFIG
		properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

		// create producer
		KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);
		// create a producer record
		ProducerRecord<String, String> record = new ProducerRecord<String, String>("first_topic", "HELLO_WORLD");
		// send data
		producer.send(record, new Callback() {

			public void onCompletion(RecordMetadata metadata, Exception e) {
				// Executes every time a record is sent or every time an exception happens
				if (e == null) {
					logger.info("Receieved new metadata :\n" + "Topic: " + metadata.topic() + "\n" + "Partition: "
							+ metadata.partition() + "\n" + "Offset: " + metadata.offset() + "\n" + "TimeStamp: "
							+ metadata.timestamp() + "\n");
				}
				else {
					logger.error("Error while sending data"+e);
				}

			}

		});
		// flush the data
		producer.flush();
		// close the producer
		producer.close();
	}

}
