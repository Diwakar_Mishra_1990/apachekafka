So as before, it's time to go hands-on just to make all these new learnings together.
And so this time, we are looking at the high throughput producer.
So, we'll add a few things. We'll add snappy message compression to our Twitter producer.
And snappy, to me, it's very helpful, because if you messages are text based,
and our Twitters, Tweets, are JSON and text based, then this is a perfect algorithm for you.
I think that snappy, personally speaking, is a good balance of cpu usage and compression ratio.
Snappy was made by Google by the way. So a lot of engineering thoughts into that compression mechanism.
And we'll also increase the batch size to 32 kilobytes and introduce a small delay through linger dot millisecond
to 20 milliseconds. So here we are, and we are going to add some high throughput settings.
So here we'll say, high throughput producer at the expense of a bit of latency and cpu usage.
So the first thing I want you to do, is properties. Set property, producer config,
and the first one is compression type config.
In TwitterProducer at line no 118 i add the following
            //High throughput producer at expense of latency and CPU usage
            properties.setProperty(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy");
            properties.setProperty(ProducerConfig.LINGER_MS_CONFIG, "20");
            properties.setProperty(ProducerConfig.BATCH_SIZE_CONFIG, Integer.toString(32*1024)); //32 kb batch size
            
And so these three settings altogether, really allow me to have a higher throughputs.
And if I run my producer now... I'm going to run it. As we'll see, we're going to get a lot of data
coming through. I can guarantee you that this data is being sent to Kafka in batches and compressed.
So it's an optimal send size. And to prove to you that the consumer doesn't need
to change anything... Well let's go see our consumer right here. Our console consumer is going a bit crazy
cause there's a lot of data coming through. But we haven't changed anything.
It knows how to decompress the messages and how to read batches and we really see that we have a quick,
a very, very quick throughputs going for these tweets. So this is how we create a very high throughput producer.

I hope you enjoyed it and I will see you in the next lecture.