package com.bitbucket.simpleStep.kafka.tutorial1;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerDemoSeekAssign {
	public static void main(String args[]) {
		// TODO Auto-generated method stub
		//System.out.println("HELLO WORLD");
		final Logger logger = LoggerFactory.getLogger(ConsumerDemoSeekAssign.class.getName());
		String bootstrapServers = "127.0.0.1:9092";
		//String groupId = "my-fifth-application";
		
		//Create consumer configs
		
		Properties properties = new Properties();
		properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		//properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
		properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		
		//Create consumer
		KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);
		
		//subscribe consumer to our topic(s)
		//consumer.subscribe(Collections.singleton("first_topic"));
		
		
		//assign and seek are mostly used to replay data or fetch a specific message
		String topic = "first_topic";
		long offsetToReadFrom = 15L;
		TopicPartition partitionToReadFrom = new TopicPartition(topic, 0);
		consumer.assign(Arrays.asList(partitionToReadFrom));
		
		//seek
		consumer.seek(partitionToReadFrom, offsetToReadFrom);
		
		int numberOfMessageToBeRead = 5;
		boolean keepOnReading = true;
		int numberOfMessageReadSoFar = 0;
		
		//poll for new data
		while(keepOnReading) {
			ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
			for(ConsumerRecord record: records) {
				logger.info("KEY: "+ record.key()+ " , Value: "+ record.value());
				logger.info("Partition: "+ record.partition()+ " , Offset: "+ record.offset());
				numberOfMessageReadSoFar++;
				if(numberOfMessageReadSoFar>=numberOfMessageToBeRead) {
					keepOnReading =false;// to exit while loop
					break;//to exit for loop
				}
			}
		}
		logger.info("Exiting the Application");
	}

}
