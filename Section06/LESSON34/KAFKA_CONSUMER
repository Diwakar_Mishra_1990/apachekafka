Okay, so now we are going to verify that the data was indeed written to Kafka
by using the Kafka console consumer. So if I do Kafka console consumer and press Enter, please refer 
01_KAFKA_CONSOLE_CONSUMER.png well we'll see again the whole documentation. You're getting used to this, right? So again as we scroll up,
we can see that there's a bootstrap server setting and it's required. So broker list before, bootstrap server now,
it's a bit confusing but it's the same, it's Kafka. Now, we can scroll down and we can see
that there is also our topic that we have to specify which is the topic we'll consume from.
So this seems like it's enough, so let's give it a try.

Kafka-console-consumer bootstrap-server = 127.0.0.1:9092 --topic first_topic

that's Kafka. And the topic is going to be first topic.

All right, we wanna consume what we have written to the first topic. Press Enter and nothing happens.
Now, isn't that odd? We have written maybe six or seven messages to our topic and here we see zero.
Now, that's a bit odd right? Well the answer is that kafka-console-consumer
just like this, does not read all the topic. It only reads from that point when you launch it
and will only intercept the new messages. So, that's a bit funky. So, let's see what that means.
On the right hand side, I just opened a new terminal, okay? And it's from using the same screen.
So, as I can launch a kafka-console-producer, and I'll create a top... and I'll produce you the topic.
first_topic. And now if I say, 
        >"Hi, how are you?."

And the produce sits on the left-hand side it just appeared. So what I said, only the messages
I'll produce right now will appear on the left-hand side. So, this is a new message. And hop, it will be on the 
left-hand side. And you can 
        >"play, play, play, play"

as much as you want with it and you'll see message will appear on the left-hand side. So, that makes sense right?
When we started console consumer, think of like our Kafka topic has like millions of messages.
We don't wanna just read the millions of messages by default. We just wanna read the new ones that arrive
because it's streaming, it's realtime. 
Please refer 03_MESSAGE_FLOW_PRODUCER_CONSUMER.png

But so how do we do to read all the messages in the topic? Because sometimes we need to. So, we launch the exact same command.
But this time, I will do from beginning. And by doing from beginning, I'm going to ask my console consumer
to go to the beginning of topic and tell me what the other is. So do from beginning, and now we see all the messages
that we have written to the topic being displayed in our consumer. Please refer 04_BEGINING_OF_PRODUCER.png
So this is pretty good, right? Again, the consumer is still running. So, it's still reading the new messages.

    >"here is another message."
    >"By the way, really awesome course."
    >"I'm super fun."

So, by the way really awesome course, we see the messages arriving as we speak. So the Kafka console consumer,
when you do from beginning will read all the messages and the one that have yet to arrive.
So, that's about it for the basics of the console consumer. So, now I'll just stop this one
and I'll just stop on the right-hand side. So now this is the basics, but in the next lecture I will show you