Okay, so now that we are ready to start programming. We've included the dependencies
of kafka in the logging. We can start writing our first producer. So, there are three steps to start our producer.

    1. So, the first thing is to create producer properties.
    2. Then we create the producer.
    3. And then we send data.

Okay, so this is all the steps we're going to implement so far. So, the first thing is to create producer properties.
            Properties properties = new Properties();

And we can just call them So, we create a new properties object. Now we need to add properties in there,
but we're not sure yet what to include. And so I'll give them to you, but I want you to know how to find them, as well.
The way to do this is to go and go to kafka documentation. And you have to always in your kafka exploration
to refer to the kafka documentation  https://kafka.apache.org/documentation/. 
And so if I scroll down, and go all the way to producer configs, this is where we're going to get our configuration.
And so we get a list of configuration we need to provide to kafka. The first one we have to provide is the key.serializer.
The second one is value.serializer. And then we have acks, and then we have bootstrap.servers, and so on.
So, we're just gonna do the required one. So, the first one is bootstrap.servers.
So, we'll do 
            properties.setProperty("bootstrap.servers", "127.0.0.1:9092");

Fairly easy. Now the second thing we have to create is the key and the value.serializer.
So, 
            properties.setProperty("key.serializer","");
		    properties.setProperty("value.serializer", "");

and we have to set something here. So, just a word before we go ahead. Key.serializer and value.serializer
basically help the producer know what type of value you're sending to kafka, and how this should be serialized to bytes.
Because kafka will convert whatever we send, the kafka client will convert whatever we send, to kafka into bytes.
Zeros and ones. So, for our case, we're going to send strings just like we did in the console producer.
So, we need string serializer for the key, and a string serializer for the value.
So, how do we find this? Well, kafka has a bunch of serializers ready for us.So, we type StringSerializer, and as 
you can see it is being hinted at me. If it's not hinted at you, it's because you haven't imported kafka correctly,
so again back to the previous lecture.
            properties.setProperty("key.serializer",StringSerializer.class.getName());
		    properties.setProperty("value.serializer", StringSerializer.class.getName());

Now, there's something you can do better. Setting these properties like this was the old way.
This was the way of hard coding, as you can see we hard code the properties on the left hand side, like these two,
and the bootstrap.servers. And then we give them a value. So, this is good, right? But there's a better way now
to have a clearer code. And it's call ProducerConfig. You see, ProducerConfig.
Which is part of organization.apache.kafka.clients.producer.

                //USING CONSTANTS IN PRODUCER_CONFIG
		        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,StringSerializer.class.getName());
		        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

This is just a better way to code in kafka and make sure you're not making any typos in this thing. So, all of this looks good,
and now we can move on to the next stage. 
So, the next stage is to create the producer. Now, to create a producer, you're going to create a KafkaProducer class.

We press TAB, and each time you press TAB

it just gives you the import on the top.

So KafkaProducer, and we'll call this producer equals with genric data being <String, String>
So, what does that mean? KafkaProducer <String, String> like this. That means that we want the key to be a string,
and the value to be a string. This is how kafka does it.
    So, <K extends Object, and V extends Object>, and so K and V can be anything you want.
But because we are producing strings right now for key and value we'll have String, String
equals new_KafkaProducer,  And in it, it takes properties as an argument, as you can see. So, we'll type in properties.
And here we go. We have created a KafkaProducer
        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);


Now, the last step is to send data. So, to send data we'll do producer dot and we'll do send.
Now the send function takes a ProducerRecord as an input. And so, we need to create that ProducerRecord.
So, let's backtrack a little bit, and the first thing that you do is to create a producer record.
So, I'll go ahead and say ProducerRecord,
        ProducerRecord<String,String> record = new ProducerRecord<String,String>("first_topic","HELLO_WORLD");
So, I will just say topic is first topic, and the value is going to be hello world.
So, now just a little bit of words on this. 
So, the code itself is first_topic, second in the value, hello world. Okay? So, if we look at ProducerRecord,
we can see that it takes two arguments. A string topic, and a V value. So, this is it.
Okay, so let's get back to our code which is right here.  So, this looks good, So, now we have a ProducerRecord,
and we'll do producer.send the record. And now, this should be alright. So, how about we give this a go,
and try and see if anything worked. For that purpose, I'm going to start a console consumer.
So, then we go here in my code, and I'm going to say kafka console consumer. Bootstrap server, the topic is first topic,
and for this I'll use my-first-application as the group ID. So, as you can see right now, the console consumer doesn't give us anything,
but that's normal because it reads from the end of the topic. So, now let's go ahead and produce some data.
And so, for this I'm going to click on the play button right here, and click on Run ProducerDemo.main.
So, it's going to run, and as you can see there's a lot of log. And it says process finished with exit code zero.
So, if you go to your consumer, well we see nothing. So, that's a bit of a surprise. We run the code, but we see nothing.
And that's another mistake that people make. When you create a producer and you send data,
this is asynchronous. Okay? And so, because it is asynchronous, this is going to happen in the background.
And so, as this is executed, basically the program exits, and the data is never sent. To wait for the data to be produced,

    producer.flush(); 
and this forced all data to be produced. Or, alternatively because it sends the end of the program, we do 
    producer.close();
Okay so, flush data. And this is flush and close producer. So, now if we run this code, we should start seeing the output we want.
So, let's run it. And now in the bottom we can see that there is a closing the Kafka producer now.
And if we go to our consumer, we should see hello world. So, we have just programmed a very simple producer
to produce data to kafka. Please refer 03_MESSAGE_RECEIEVED.png
So, congratulations. That's your first step, and I'll see you in the next lecture.