package com.bitbucket.simpleStep.kafka.tutorial1;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerDemoWithThreads {
	final static Logger logger = LoggerFactory.getLogger(ConsumerDemoWithThreads.class.getName());
	public static void main(String args[]) {
		new ConsumerDemoWithThreads().run();
	}
	public ConsumerDemoWithThreads() {}
	
	public void run() {
		String bootstrapServers = "127.0.0.1:9092";
		String groupId = "my-sixth-application";
		String topic ="first_topic";
		CountDownLatch latch = new CountDownLatch(1);
		Runnable myConsumerThread = new ConsumerThread(latch,
														topic,
														bootstrapServers,
														groupId);
		//start a thread
		Thread myThread = new Thread(myConsumerThread);
		myThread.start();
		//add a shutdownhook 
		Runtime.getRuntime().addShutdownHook(
				new Thread(
						() ->{
							logger.info("Caught shutdown hook");
							((ConsumerThread)myConsumerThread).shutDown();
							try {
								latch.await();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							logger.info("Application has exited");
						}
						));
		try {
			latch.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

class ConsumerRunnable implements Runnable {
	CountDownLatch latch;
	final Logger logger = LoggerFactory.getLogger(ConsumerThread.class.getName());
	private KafkaConsumer<String, String> consumer;

	public ConsumerRunnable(CountDownLatch latch, String topic, String bootstrapServers, String groupId) {
		this.latch = latch;
		// Create consumer configs

		Properties properties = new Properties();
		properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
		properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

		// create consumer
		consumer = new KafkaConsumer<String, String>(properties);
		// subscribe consumer of topics
		consumer.subscribe(Arrays.asList(topic));
	}

	public void run() {
		try {

			// poll for new data
			while (true) {
				ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
				for (ConsumerRecord record : records) {
					logger.info("KEY: " + record.key() + " , Value: " + record.value());
					logger.info("Partition: " + record.partition() + " , Offset: " + record.offset());
				}
			}
		} catch (WakeupException e) {
			logger.info("Receieved Shut Down Command");
		} finally {
			consumer.close();
			//tell our main code we are done
			latch.countDown();
		}

	}

	public void shutDown() {
		// the wakeup method is a special method to interrupt consumer.poll()
		// it will throw the exception and exception is called wakeUp exception
		consumer.wakeup();
	}

}

